/* CLUNC
 * Copyright 2011-2012 Thomas Monjalon
 * Licensed under the GPLv3
 * See http://www.gnu.org/licenses/gpl.html
 *
 * Common defines
 */

# ifndef COMMON_H
# define COMMON_H

# include <stdio.h>
# include <string.h>

# define eprintf(fmt, args...) fprintf (stderr, fmt "\n", ## args)

# define STR_LEN(x) x, strlen (x)
# define PTR_SIZE(x) &x, sizeof x
# define VPTR_SIZE(x) (void *) PTR_SIZE (x)

# endif /* COMMON_H */
